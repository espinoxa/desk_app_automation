package co.com.proyectobase.screenplay.interactions;

import java.util.List;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.targets.Target;

public class Selection implements Interaction {

	private Target list;
	private String option;
	
	public Selection(Target list, String option) {
		
		this.list = list;
		this.option = option;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		List<WebElement> listObject = list.resolveFor(actor).findElements(By.tagName("option"));
		for(int i=0; i < listObject.size(); i++){
			if (listObject.get(i).getText().equals(option)) {
				listObject.get(i).click();
				break;
			}			 
		}
	}
}

