package co.com.proyectobase.screenplay.tasks;

import static co.com.proyectobase.screenplay.userinterface.GemaPage.CLAVE;
import static co.com.proyectobase.screenplay.userinterface.GemaPage.INGRESAR;
import static co.com.proyectobase.screenplay.userinterface.GemaPage.USUARIO;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.winium.actions.ClickDesk;
import net.serenitybdd.screenplay.winium.actions.EnterDesk;

public class Realiza implements Task{
	
	private String usuario, clave;


	public Realiza(String usuario, String clave) {
		super();
		this.usuario = usuario;
		this.clave = clave;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(EnterDesk.theValue(usuario).into(USUARIO));
		actor.attemptsTo(EnterDesk.theValue(clave).into(CLAVE));
		actor.attemptsTo(ClickDesk.on(INGRESAR));		
	}

	public static Performable laAutenticacion(String usuario, String clave) {
		return Tasks.instrumented(Realiza.class, usuario, clave);
	}

}
