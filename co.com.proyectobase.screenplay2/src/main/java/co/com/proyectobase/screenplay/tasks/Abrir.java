package co.com.proyectobase.screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.winium.actions.OpenDesk;

public class Abrir implements Task{

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(OpenDesk.browserOnDesk("C:\\Program Files\\Accenture\\GEMA\\BC.Ada.Presentacion.exe"));		
	}

	public static Performable elAplicativo() {
		return Tasks.instrumented(Abrir.class);
	}

}
