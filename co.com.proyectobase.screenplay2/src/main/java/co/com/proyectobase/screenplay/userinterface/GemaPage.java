package co.com.proyectobase.screenplay.userinterface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.winium.targets.TargetDesktop;

public class GemaPage {
	
	public static final TargetDesktop USUARIO = TargetDesktop.the("Campo usuario")
			.located(By.id("txtUsuario"));
	public static final TargetDesktop CLAVE = TargetDesktop.the("Campo clave")
			.located(By.id("txtClave")); 
	public static final TargetDesktop INGRESAR = TargetDesktop.the("Bot�n ingresar")
			.located(By.id("btnAceptar")); 

}
