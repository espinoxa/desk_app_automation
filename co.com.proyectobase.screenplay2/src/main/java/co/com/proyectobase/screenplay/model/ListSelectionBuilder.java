package co.com.proyectobase.screenplay.model;

import co.com.proyectobase.screenplay.interactions.Selection;
import net.serenitybdd.screenplay.targets.Target;

public class ListSelectionBuilder {

	private Target list;
	
	public ListSelectionBuilder(Target list) {
		this.list=list;
	}

	public Selection theOption(String option)
	{
		return new Selection(list, option);
		
	}
	
}
