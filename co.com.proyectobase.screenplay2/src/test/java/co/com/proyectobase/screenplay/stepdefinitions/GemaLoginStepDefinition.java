package co.com.proyectobase.screenplay.stepdefinitions;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Realiza;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.winium.abilities.Use;

public class GemaLoginStepDefinition {
	
  @Before
    public void prepareStage(){
        OnStage.setTheStage(new OnlineCast());
        theActorCalled("Rafa").can(Use.DesktopApplications());
    }
	
	@Given("^que Rafa quiene acceder a GENA$")
	public void queRafaQuieneAccederAGENA() throws Exception {
		theActorInTheSpotlight().wasAbleTo(Abrir.elAplicativo());	 
	}

	@When("^el escribe \"([^\"]*)\" y \"([^\"]*)\"$")
	public void elEscribeY(String usuario, String clave) throws Exception {
		theActorInTheSpotlight().attemptsTo(Realiza.laAutenticacion(usuario, clave ));
	 
	}

	@Then("^el accede a GEMA$")
	public void elAccedeAGEMA() throws Exception {
	 
	}

}
